create table public.t_channel (
  pk integer not null primary key generated always as identity,
  title text not null,
  link text,
  description text not null,
  language text,
  copyright text,
  pubdate timestamp without time zone not null
);

create table public.t_item (
  pk integer not null primary key generated always as identity,
  fk_channel integer not null references public.t_channel(pk),
  title text not null,
  description text not null,
  link text,
  author text,
  pubdate timestamp without time zone not null
);

create view public.v_rss as
with x(fk_channel, rss) as (
  select t_item.fk_channel,
         string_agg('<item><title>' || t_item.title) || '</title><description>' || t_item.description || '</description><link>'
                    || t_item.link || '</link><author>' || t_item.author || '</author><guid>' || t_item.fk_channel || '_' || t_item.pk
                    || '</guid><pubDate>' || t_item.pubdate || '</pubDate></item>', '' order by t_item.pubdate) as string_agg
  from public.t_item
  group by t_item.fk_channel
) select c.title as feedname,
         '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel><title>' || c.title || '</title><link>' || c.link 
         || '</link><description>' || c.description || '</description><language>' || c.language || '</language><copyright>' 
         || c.copyright || '</copyright><pubDate>' || c.pubdate || '</pubDate>' || x.rss || '</channel></rss>' as feed
from public.t_channel c
left join x on x.fk_channel = c.pk;
